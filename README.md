# Us26 Week5


# Rust AWS Lambda Project

This project demonstrates the use of Rust for serverless computing with AWS Lambda, integrated with DynamoDB. The Lambda function fetches employee names from a DynamoDB database based on specified criteria such as company and role.

## Functionality

The Lambda function responds to requests with employee ID and name based on provided company and role parameters.

## Setup

To set up the project, follow these steps:

1. Initialize the Cargo Lambda project:
    ```
    cargo lambda new <YOUR-PROJECT-NAME>
    ```

2. Modify the `Cargo.toml` and `src/main.rs` files according to your design and requirements.

3. Test the functionality locally:
    ```
    cargo lambda watch
    ```

4. Deploy the project to AWS:
    ```
    cargo lambda build --release
    cargo lambda deploy --region us-east-2 --iam-role <description>
    ```

## API Gateway Setup

1. Create a trigger for the Lambda function, i.e., API Gateway for the function.

2. Create a REST API with a new resource, e.g., `week5_db`, and add an ANY method to it.

3. Deploy the API, creating a new stage, e.g., `test`.

4. Find the invoke URL under the newly created stage.

## AWS DynamoDB Setup

1. Attach the following policies to the role associated with the Lambda function: `AmazonDynamoDBFullAccess`, `AWSLambda_FullAccess`, and `AWSLambdaBasicExecutionRole`.

2. Create a DynamoDB table named `EmployeeInfo`.

3. Populate the table with sample data. For example:
    ```json
    {
      "employee_id": "001",
      "name": "John Doe",
      "company": "Example Corp",
      "role": "Engineer"
    }
    ```

## Results

![Screenshot](images/1.png)

![Screenshot](images/2.png)

![Screenshot](images/3.png)

![Screenshot](images/4.png)



## License

This project is licensed under the [MIT License](LICENSE).
